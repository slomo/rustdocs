(function() {var implementors = {};
implementors["gst_plugin"] = [{text:"impl <a class=\"trait\" href=\"https://doc.rust-lang.org/nightly/core/convert/trait.From.html\" title=\"trait core::convert::From\">From</a>&lt;<a class=\"enum\" href=\"gst_plugin/error/enum.FlowError.html\" title=\"enum gst_plugin::error::FlowError\">FlowError</a>&gt; for FlowReturn",synthetic:false,types:["gstreamer::auto::enums::FlowReturn"]},{text:"impl&lt;'a&gt; <a class=\"trait\" href=\"https://doc.rust-lang.org/nightly/core/convert/trait.From.html\" title=\"trait core::convert::From\">From</a>&lt;&amp;'a <a class=\"enum\" href=\"gst_plugin/error/enum.FlowError.html\" title=\"enum gst_plugin::error::FlowError\">FlowError</a>&gt; for FlowReturn",synthetic:false,types:["gstreamer::auto::enums::FlowReturn"]},];

            if (window.register_implementors) {
                window.register_implementors(implementors);
            } else {
                window.pending_implementors = implementors;
            }
        
})()
